package pl.uz_ztus.front_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableEurekaClient
@EnableEurekaServer
@SpringBootApplication
public class FrontApiApplication {

    public static void main(final String[] args) {
        SpringApplication.run(FrontApiApplication.class, args);
    }
}

